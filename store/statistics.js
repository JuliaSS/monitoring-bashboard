export const state = () => ({
    monitoringUrl: [],
    getDate: []
  })
  export const mutations = {
    SET_MONITORING_URL(state, statistics) {
      state.monitoringUrl = statistics
    },
    SET_DATE(state, statistics) {
      state.getDate = statistics
    }
  }
  import axios from 'axios';
  export const actions = {
    async fetchMonitoring({commit}, date){
          const statisticsList = await axios.get(`http://91.223.123.199:9216/api/v1/get-prometheus-metric?start=${date[0]} ${date[1]}&stop=${date[2]} ${date[3]}`)
          // console.log('store', statisticsList)
          commit('SET_MONITORING_URL', statisticsList.data.data)
    },
    setDate({commit}, date) {
      commit('SET_DATE', date)
    },
  }
  export const getters = {
    MONITORING_URL:s=>s.monitoringUrl,
    GET_DATE:s=>s.getDate
  }